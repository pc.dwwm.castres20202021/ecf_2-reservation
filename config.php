<?php include 'data.php';

error_reporting(E_ALL);
ini_set('display_errors', 'On');

function rentrer() {
 $nom = $_POST["nom"];
 $tel = $_POST["tel"];
 $mail = $_POST["mail"];
 $dates = date('Y:m:d H:i:s', strtotime($_POST['dates']));

try{
  
    global $dbco;
    $sth = $dbco->prepare("
        INSERT INTO Users(nom, tel, mail, dates) VALUES(:nom, :tel, :mail,:dates)");
    $sth->bindParam(':nom',$nom);
    $sth->bindParam(':tel',$tel);
    $sth->bindParam(':mail',$mail);
    $sth->bindParam(':dates',$dates);
    $sth->execute();
    
   
    
}
catch(PDOException $e){
    echo 'Impossible de traiter les données. Erreur : '.$e->getMessage();
 }
}
//////////////////////////////////////////////////////////////////////////////

function getClients(){
    global $dbco;
    
try{
    $sth = $dbco->prepare("SELECT nom, tel, mail, dates FROM Users ORDER BY dates");
    $sth-> execute();
    
    $resultat = $sth->fetchAll(PDO::FETCH_ASSOC);
    return $resultat;
  
  }
        
  catch(PDOException $e){
    echo "Erreur : " . $e->getMessage();
  }
}
/////////////////////////////////////////////////////////////////////////

function dateTri(){
    global $dbco;
try{
    $sth = $dbco->prepare("SELECT dates FROM Users ORDER BY ASC");
    $sth-> execute();

    $rdates = $sth->fetchall(PDO::FETCH_ASSOC);
    return $rdates;
}
catch(PDOException $e){
    echo "Erreur : " . $e->getMessage();
  }
}

?>
