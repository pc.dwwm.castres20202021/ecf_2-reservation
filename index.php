<?php  
require 'config.php' ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="main.js"></script>
    

    <title>Accueil</title>
</head>
<html>
    <head>
       <meta charset="utf-8">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-12">
                  <div class="d-flex justify-content-center">
                    <h1>Formulaire d'inscription</h1>
                  </div>
                  <div class="d-flex justify-content-center bg-secondary">
                    <form class ="" action="" method="POST">
                        <input class="d-flex justify-content-center" type="text" placeholder="Entrez votre nom" name="nom" required>
                        <input class="d-flex justify-content-center" type="text" placeholder="Téléphone" name="tel" required>
                        <input class="d-flex justify-content-center" type="text" placeholder="Entrez votre mail" name="mail" required>

                        <label for="dates">Veuillez saisir une date et une heure pour le rendez-vous :</label>
                        <input id="dates" type="datetime-local" name="dates" value="2021-08-05T09:00">  
                        
                        <input type="submit" name="submit" id='submit' value='Réserver'>
                    </form>
                  </div>
                   <?php  if(isset($_POST['submit'])){rentrer(); } ?>
                </div>
            </div>
        </div>
    </body>
</html>